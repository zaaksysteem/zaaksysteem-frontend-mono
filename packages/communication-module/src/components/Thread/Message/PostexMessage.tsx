// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from '@material-ui/core';
import { ExternalMessageType } from '../../../types/Message.types';
import MessageHeader from './MessageHeader';
import MessageContent from './MessageContent';
import { usePostexMessageStyle } from './PostexMessage.style';
import MessageSenderIcon from './MessageSenderIcon/MessageSenderIcon';
import CreateExternalMessageTitle from './library/CreateExternalMessageTitle';

type PostexMessagePropsType = {
  message: ExternalMessageType;
  caseNumber?: string;
  expanded?: boolean;
  isUnread: boolean;
};

/* eslint complexity: [2, 7] */
const PostexMessage: React.FunctionComponent<PostexMessagePropsType> = ({
  message,
  isUnread,
  expanded = false,
}) => {
  const {
    content,
    createdDate,
    sender,
    type,
    attachments,
    participants,
    id,
    hasSourceFile,
    failureReason,
  } = message;
  const classes = usePostexMessageStyle();
  const icon = <MessageSenderIcon type={sender ? sender.type : 'person'} />;
  const title = CreateExternalMessageTitle(message);
  const [t] = useTranslation('communication');
  const [isExpanded, setExpanded] = useState(isUnread || expanded);

  const formattedParticipants = participants?.to.map(pt => pt.name).join(', ');
  const headerName = 'thread.postexMessageHeader.recipient';
  const participantTextLines = formattedParticipants
    ? [`${t(headerName)} : ${formattedParticipants}`]
    : [];

  const info = participantTextLines.reduce<string>(
    (acc, line) =>
      line
        ? `${acc}
    ${line}`
        : acc,
    ''
  );

  return (
    <ExpansionPanel
      classes={{ root: classes.panel }}
      defaultExpanded={isExpanded}
    >
      <ExpansionPanelSummary
        onClick={() => setExpanded(!isExpanded)}
        classes={{
          root: classes.summary,
          content: classes.content,
          expanded: classes.expanded,
        }}
      >
        <MessageHeader
          date={createdDate}
          title={title}
          icon={icon}
          info={info}
          isUnread={isUnread}
          id={id}
          hasSourceFile={hasSourceFile}
          hasAttachment={Boolean(attachments.length)}
          failureReason={failureReason}
        />
      </ExpansionPanelSummary>
      <ExpansionPanelDetails classes={{ root: classes.details }}>
        <MessageContent
          content={content}
          attachments={attachments}
          type={type}
        />
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
};

export default PostexMessage;
