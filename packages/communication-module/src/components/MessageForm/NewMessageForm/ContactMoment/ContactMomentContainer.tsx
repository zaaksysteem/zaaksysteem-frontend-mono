// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { saveContactMoment } from '../../../../store/add/communication.add.actions';
import { CommunicationRootStateType } from '../../../../store/communication.reducer';
import GenericMessageForm from '../../GenericMessageForm/GenericMessageForm';
import { SaveContactMomentFormValuesType } from '../../../../types/Message.types';
import { GenericMessageFormPropsType } from '../../GenericMessageForm/GenericMessageForm.types';
import getContactMomentFormDefinition from './contactMoment.formDefinition';

type PropsFromStateType = Pick<
  GenericMessageFormPropsType<SaveContactMomentFormValuesType>,
  'formDefinition' | 'busy' | 'formName'
>;

type PropsFromDispatchType = Pick<
  GenericMessageFormPropsType<SaveContactMomentFormValuesType>,
  'save'
>;

type PropsFromOwnType = Pick<GenericMessageFormPropsType, 'cancel'>;

const mapStateToProps = (
  stateProps: CommunicationRootStateType
): PropsFromStateType => {
  const {
    communication: {
      add: { state },
      context,
    },
  } = stateProps;

  return {
    formDefinition: getContactMomentFormDefinition(context),
    busy: state === AJAX_STATE_PENDING,
    formName: 'contact-moment',
  };
};

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => {
  return {
    save(values) {
      dispatch(saveContactMoment({ values }) as any);
    },
  };
};

const connected = connect<
  PropsFromStateType,
  PropsFromDispatchType,
  PropsFromOwnType,
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(GenericMessageForm);

export default connected;
