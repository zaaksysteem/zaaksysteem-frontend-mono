// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const capitalize = string =>
  string.replace(/^./, function Upper(match) {
    return match.toUpperCase();
  });
