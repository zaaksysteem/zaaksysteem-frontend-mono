// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type TranslationKeysType =
  | 'form:choose'
  | 'form:loading'
  | 'form:beginTyping'
  | 'form:creatable'
  | 'form:create';
export type TranslationsType = { [k in TranslationKeysType]: string };
