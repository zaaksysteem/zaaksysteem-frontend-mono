// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Theme } from '@mintlab/ui/types/Theme';
import { ReactSelectSubcomponentNames } from '../types/ReactSelectSubcomponentNames';

export type StyleSheetCreatorType = (arg: {
  theme: Theme;
  focus?: boolean;
  error?: any;
}) => {
  [k in ReactSelectSubcomponentNames]?: (base: any, state: any) => any;
};
