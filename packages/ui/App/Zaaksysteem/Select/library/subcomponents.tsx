// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { components } from 'react-select';
import { MultiValueGenericProps } from 'react-select/src/components/MultiValue';
import CloseIndicator from '../../../Shared/CloseIndicator/CloseIndicator';
import DropdownIndicatorButton from '../../../Shared/DropdownIndicator/DropdownIndicator';
import { MultiValueLabelIconType } from '../types/BaseSelectPropsType';

interface MultiValueLabelPropsType extends MultiValueGenericProps<any> {
  selectProps: {
    multiValueLabelIcon: MultiValueLabelIconType;
  };
}

const ClearIndicator = (props: any) => (
  <components.ClearIndicator {...props}>
    <CloseIndicator />
  </components.ClearIndicator>
);

const DropdownIndicator = (props: any) => {
  const { options } = props;
  return Array.isArray(options) && options.length ? (
    <components.DropdownIndicator {...props}>
      <DropdownIndicatorButton />
    </components.DropdownIndicator>
  ) : null;
};

const Control = ({
  selectProps: { startAdornment },
  children,
  ...rest
}: any) => {
  return (
    <components.Control {...rest}>
      {Boolean(startAdornment) && (
        <div className="startAdornment">{startAdornment}</div>
      )}
      {children}
    </components.Control>
  );
};

const MultiValueLabel = (props: MultiValueLabelPropsType) => {
  const {
    selectProps: { multiValueLabelIcon },
    data: option,
  } = props;

  return (
    <>
      {multiValueLabelIcon ? multiValueLabelIcon({ option }) : null}
      <components.MultiValueLabel {...props} />
    </>
  );
};

export const subcomponents = {
  ClearIndicator,
  DropdownIndicator,
  Control,
  MultiValueLabel,
};
