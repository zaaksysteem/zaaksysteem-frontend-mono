// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
// eslint-disable-next-line import/no-unresolved
import toJson from 'enzyme-to-json';
// eslint-disable-next-line import/no-unresolved
import { shallow } from 'enzyme';
import { VerticalMenu } from './VerticalMenu';

/**
 * @test {VerticalMenu}
 */
describe('The `VerticalMenu` component', () => {
  test('renders correctly', () => {
    const items = [{ label: 'foo' }, { label: 'bar' }];
    const component = shallow(<VerticalMenu items={items} classes={{}} />);

    expect(toJson(component)).toMatchSnapshot();
  });
});
