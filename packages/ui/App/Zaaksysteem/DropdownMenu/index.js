// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as DropdownMenu } from './DropdownMenu';
export * from './DropdownMenu';
export default DropdownMenu;
