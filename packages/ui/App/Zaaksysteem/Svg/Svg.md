# 🔌 `Svg` component

> [DWIM](http://www.catb.org/jargon/html/D/DWIM.html)
  SVG component. 
  

## Usage

The component is used *instead* of the `svg` element and creates a container element.

    <Svg width="400px" viewBox={[200, 100]}>
        <circle cx="50" cy="50" r="50"/>
    </Svg>

produces

    <span style="width:400px;height:200px" class="CSS_Module_ID">
      <svg viewBox="0 0 200 100">
        <circle cx="50" cy="50" r="50"/>
      </svg>
    </span>
    
## Props

### `viewBox` (`number`|`Array`)

`viewBox` is the only required property. It is used to configure the
[SVG `viewBox` attribute](https://www.w3.org/TR/SVG11/coords.html#ViewBoxAttribute).

- If it is a `number`, that number is used as both `<width>` and `<height>`, 
  and `<min-x>` and `<min-y>` are set to `0`. 
- If it is an `array` of two numbers, those are used as `<width>` and `<height>`, respectively,
  and `<min-x>` and `<min-y>` are set to `0`.
- If it is an `array` of four numbers, those are used as `<min-x>`, `<min-y>`, `<width>` and `<height>`, respectively.

If none of `width`, `height` and `className` are set, `<width>` and `<height>` 
are used to set the size of the container element with the `px` unit. Example:

    <Svg viewBox={48}>
      <!-- SVG elements -->
    </Svg>

produces   

    <span style="width:48px;height:48px" class="CSS_Module_ID">
      <svg viewBox="0 0 48 48">
        <!-- SVG elements -->
      </svg>
    </span>

### `width` / `height` (`string`)

`width` and `height` are 
[CSS dimensions](https://www.w3.org/TR/css-values-3/#dimensions). 
While you *can* set both,
the normal use case is setting one and letting the other be 
calculated according to the `viewBox` value.

In the latter case, if the unit is `%`, 
[a cross-browser hack to make SVGs responive](https://tympanus.net/codrops/2014/08/19/making-svgs-responsive-with-css/)
is applied.

#### Supported units:

- [%](https://www.w3.org/TR/css-values-3/#percentages)
- [em](https://www.w3.org/TR/css-values-3/#em)
- [ex](https://www.w3.org/TR/css-values-3/#ex)
- [px](https://www.w3.org/TR/css-values-3/#px)
- [rem](https://www.w3.org/TR/css-values-3/#rem)
- [vh](https://www.w3.org/TR/css-values-3/#vh)
- [vw](https://www.w3.org/TR/css-values-3/#vw)

### `className` (`string`)

If neither `width` nor `height` are set, `className` is 
assumed to take care of the size of the container element.

This is useful for integration with third party CSS libraries.

## See also

- [`Svg` stories](/npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Svg)
- [`Svg` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#zaaksysteem-svg)
