// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the ErrorLabel component
 * @param {Object} theme
 * @return {JSS}
 */
export const errorLabelStyleSheet = ({ palette: { error } }) => ({
  root: {
    color: error.main,
  },
});
