// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, text } from '../../story';
import Paper from '.';

stories(module, __dirname, {
  Default() {
    return <Paper scope="story">{text('Greeting', 'Hello, world!')}</Paper>;
  },
});
