# 🔌 `Button` component

> *Material Design* **Button**.

## Example

    <Button
      presets=['primary', 'contained']
    >Don't press me!</Button>

## See also

- [`Button` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/Button)
- [`Button` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-button)

## External resources

- [*Material Guidelines*: Buttons](https://material.io/design/components/buttons.html)
- [*Material-UI* `Button` API](https://material-ui.com/api/button/)
- [*Material-UI* `IconButton` API](https://material-ui.com/api/icon-button/)
