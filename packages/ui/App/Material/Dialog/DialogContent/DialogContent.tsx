// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MUIDialogContent from '@material-ui/core/DialogContent';
import { useDialogContentStyles } from './DialogContent.style';

export const DialogContent: React.ComponentType<{ padded?: boolean }> = ({
  children,
  padded = true,
}) => {
  const classes = useDialogContentStyles();

  return (
    <MUIDialogContent
      classes={{
        root: padded ? classes.padded : classes.root,
      }}
    >
      {children}
    </MUIDialogContent>
  );
};
