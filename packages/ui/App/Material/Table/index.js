// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export { default as Table } from '@material-ui/core/Table';
export { default as TableRow } from './row/TableRow';
export { default as TableCell } from './cell/TableCell';
export { default as TableHead } from '@material-ui/core/TableHead';
export { default as TableBody } from '@material-ui/core/TableBody';
export { default as TableFooter } from '@material-ui/core/TableFooter';
