// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type DatePickerPropsType = {
  value: Pick<DatePickerProps, 'value'>;
  onChange: (date: Date) => void;
  name: string;
  outputFormat: string;
  displayFormat: string;
  [key: string]: any;
};
