// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const SHOW_DIALOG = 'SHOW_DIALOG';
export const HIDE_DIALOG = 'HIDE_DIALOG';
