// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  AnyFormDefinitionField,
  FormDefinition,
  FormShapeType,
} from '../types';
import { isSteppedForm } from './formHelpers';

function findFieldByName<Values>(
  fields: AnyFormDefinitionField<Values>[],
  name?: keyof Values
): AnyFormDefinitionField<Values> {
  // Since we force the name to always be a keyof Values, we can "safely" assume this will always return a value
  return fields.find(
    item => item.name === name
  ) as AnyFormDefinitionField<Values>;
}

function getAllFields<Values>(
  formDefinition: FormDefinition<Values>
): AnyFormDefinitionField<Values>[] {
  return isSteppedForm<Values>(formDefinition)
    ? formDefinition.reduce<AnyFormDefinitionField<Values>[]>(
        (acc, step) => [...acc, ...step.fields],
        []
      )
    : formDefinition;
}

export function getAllFieldsAsObject<Values>(
  formDefinition: FormDefinition<Values>
) {
  const allFields = getAllFields(formDefinition);
  const obj = {} as FormShapeType<keyof Values>;

  allFields.forEach(field => (obj[field.name] = field));

  return obj;
}

export function getFieldByName<Values>(
  formDefinition: FormDefinition<Values>,
  name?: keyof Values
): AnyFormDefinitionField<Values> {
  const allFields = getAllFields(formDefinition);
  return findFieldByName<Values>(allFields, name);
}

export default getFieldByName;
