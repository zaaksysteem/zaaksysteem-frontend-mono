// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  FormDefinition,
  FormValuesType,
  AnyFormDefinitionField,
} from '../types/formDefinition.types';
import { isSteppedForm } from './formHelpers';

function getValuesFromFields<FormShape>(
  fields: AnyFormDefinitionField<FormShape>[]
) {
  return fields.reduce<FormShape>(
    (values, field) => ({
      ...values,
      [field.name]: field.value,
    }),
    {} as FormValuesType<FormShape>
  );
}

export default function getValuesFromDefinition<FormShape = any>(
  formDefinition: FormDefinition<FormShape>
): FormShape {
  return isSteppedForm(formDefinition)
    ? formDefinition.reduce(
        (acc, step) => ({
          ...acc,
          ...getValuesFromFields(step.fields),
        }),
        {} as FormShape
      )
    : getValuesFromFields(formDefinition);
}
