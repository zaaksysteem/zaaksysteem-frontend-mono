// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

//@ts-nocheck

import { hasValue, valueEquals, valueOneOf, isTruthy } from './conditions';

describe('Rule engine', () => {
  describe('conditions', () => {
    describe('hasValue', () => {
      test('should return true when given a value', () => {
        expect(hasValue({ value: 'test' })).toEqual(true);
        expect(hasValue({ value: 0 })).toEqual(true);
        expect(hasValue({ value: { value: 'test' } })).toEqual(true);
        expect(hasValue({ value: { value: 0 } })).toEqual(true);
      });
      test('should return false when not given a value', () => {
        expect(hasValue('')).toEqual(false);
        expect(hasValue({ value: '' })).toEqual(false);
        expect(hasValue({ value: {} as any })).toEqual(false);
        expect(hasValue({ value: null as any })).toEqual(false);
      });
    });

    describe('valueEquals', () => {
      const valueEqualsFoo = valueEquals('Foo');

      test('should return true when given value is equal', () => {
        expect(valueEqualsFoo({ value: 'Foo' })).toEqual(true);
        expect(valueEqualsFoo({ value: { value: 'Foo' } })).toEqual(true);
      });
      test('should return false when given value is not equal', () => {
        expect(valueEqualsFoo({ value: 'Bar' })).toEqual(false);
        expect(valueEqualsFoo({ value: { value: 'Bar' } })).toEqual(false);
      });
    });

    describe('valueOneOf', () => {
      const oneOf = valueOneOf(['Foo', 'Test']);

      test('should return true when given value is equal', () => {
        expect(oneOf({ value: 'Foo' })).toEqual(true);
        expect(oneOf({ value: { value: 'Foo' } })).toEqual(true);
      });
      test('should return false when given value is not equal', () => {
        expect(oneOf({ value: 'Bar' })).toEqual(false);
        expect(oneOf({ value: { value: 'Bar' } })).toEqual(false);
      });
    });

    describe('isTruthy', () => {
      test('should return true when given value is truthy', () => {
        expect(isTruthy(true)).toEqual(true);
        expect(isTruthy(1)).toEqual(true);
        expect(isTruthy('hawkeye is a superhero')).toEqual(true);
        expect(isTruthy({})).toEqual(true);
        expect(isTruthy([])).toEqual(true);
      });
      test('should return false when given value is not truthy', () => {
        expect(isTruthy(false)).toEqual(false);
        expect(isTruthy(0)).toEqual(false);
        expect(isTruthy('')).toEqual(false);
        expect(isTruthy(null)).toEqual(false);
        expect(isTruthy(undefined)).toEqual(false);
        expect(isTruthy(NaN)).toEqual(false);
      });
    });
  });
});
