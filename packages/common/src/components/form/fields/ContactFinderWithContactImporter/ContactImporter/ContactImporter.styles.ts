// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => {
  return {
    wrapper: {
      display: 'flex',
      flexWrap: 'wrap',
      alignContent: 'flex-start',
      padding: '0px 20px 4px 20px',
    },
  };
});
