// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement, APICatalog } from '@zaaksysteem/generated';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';

export type CaseTypeType = {
  id: string;
  name: string;
  initiator_source: string;
  type_of_requestors: (
    | 'niet_natuurlijk_persoon'
    | 'preset_client'
    | 'natuurlijk_persoon_na'
    | 'natuurlijk_persoon'
    | 'medewerker'
  )[];
  preset_requestor: ValueType<string> | null;
};

export interface CaseTypeOptionType extends ValueType<string> {
  fetched?: boolean;
  data: CaseTypeType;
}

export const fetchCaseType = async (
  case_type_uuid: string,
  openServerErrorDialog: OpenServerErrorDialogType
): Promise<CaseTypeType> => {
  const body =
    await request<APICaseManagement.GetCaseTypeActiveVersionResponseBody>(
      'GET',
      buildUrl<APICaseManagement.GetCaseTypeActiveVersionRequestParams>(
        '/api/v2/cm/case_type/get_active_version',
        { case_type_uuid }
      )
    ).catch(openServerErrorDialog);

  return body && body?.data
    ? {
        id: case_type_uuid,
        name: body.data.attributes.name,
        type_of_requestors: body.data.attributes.requestor.type_of_requestors,
        initiator_source: body.data.attributes.initiator_source,
        preset_requestor:
          body.data.relationships?.preset_requestor?.data || null,
      }
    : {
        id: '',
        name: '',
        type_of_requestors: [],
        initiator_source: '',
        preset_requestor: null,
      };
};

export const fetchCaseTypeChoices =
  (
    openServerErrorDialog: OpenServerErrorDialogType,
    includeOffline: boolean,
    type?: SubjectTypeType
  ) =>
  async (keyword: string): Promise<CaseTypeOptionType[]> => {
    if (!keyword) return [];

    const getChoicesIncludeOffline = async () => {
      const res = await request<APICatalog.SearchCatalogResponseBody>(
        'GET',
        buildUrl<APICatalog.SearchCatalogRequestParams>(
          '/api/v2/admin/catalog/search',
          { keyword, 'filter[type]': 'case_type' }
        )
      ).catch(openServerErrorDialog);

      return res
        ? res.data.map(({ id, attributes: { name } }) => ({
            value: id,
            data: {
              id,
              name,
              type_of_requestors: [],
              initiator_source: '',
              preset_requestor: null,
            },
            label: name,
            fetched: false,
          }))
        : [];
    };

    const getChoicesExcludeOffline = async () => {
      const res = await request<APICatalog.SearchCatalogResponseBody>(
        'GET',
        buildUrl<APICaseManagement.SearchRequestParams>('/api/v2/cm/search', {
          keyword,
          type: 'case_type' as any,
          ...(type && {
            'filter[relationships.case_type.requestor_type]': type,
          }),
        })
      ).catch(openServerErrorDialog);

      return res
        ? res.data.map(({ id, meta: { summary } }) => ({
            value: id,
            data: {
              id,
              name: summary,
              type_of_requestors: [],
              initiator_source: '',
              preset_requestor: null,
            },
            label: summary,
            fetched: false,
          }))
        : [];
    };

    return includeOffline
      ? getChoicesIncludeOffline()
      : getChoicesExcludeOffline();
  };

export const saveCaseTypeToRemember = (caseTypeUuid?: string) => {
  const remember = Boolean(caseTypeUuid);

  request('POST', '/api/user/settings', {
    remember_casetype: {
      casetype_id: caseTypeUuid,
      remember,
    },
  });
};
