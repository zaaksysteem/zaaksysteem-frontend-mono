// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { Select } from '@mintlab/ui/App/Zaaksysteem/Select';
//@ts-ignore
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { FormFieldComponentType } from '../../types/Form2.types';
import { CaseRoleFinderConfigType } from './CaseRoleFinder.types';
import { fetchCaseRoleChoices } from './CaseRoleFinder.library';

const CaseRoleFinder: FormFieldComponentType<
  ValueType<string>,
  CaseRoleFinderConfigType
> = ({ multiValue, config, value, ...restProps }) => {
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  return (
    <React.Fragment>
      <DataProvider
        providerArguments={[config]}
        autoProvide={true}
        provider={fetchCaseRoleChoices(openServerErrorDialog)}
      >
        {({ data, busy }) => {
          return (
            <Select
              {...restProps}
              value={value}
              choices={data || []}
              isClearable={true}
              loading={busy}
              isMulti={multiValue}
              components={
                {
                  Option: MultilineOption,
                } as any
              }
              filterOption={() => true}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};

export default CaseRoleFinder;
