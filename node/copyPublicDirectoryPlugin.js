// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const path = require('path');
const fs = require('fs-extra');

module.exports = appName => {
  const p1 = path.resolve(__dirname, '..', 'apps', appName, 'public');
  const p2 = path.resolve(__dirname, '..', 'build', 'apps', appName);

  return {
    apply(compiler) {
      compiler.hooks.done.tap('PublicDirectoryPlugin', () => {
        fs.copySync(p1, p2, {
          recursive: true,
          filter: path => !path.includes('index.html'),
        });
      });
    },
  };
};
