// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
//@ts-ignore
import { objectifyParams } from '@mintlab/kitchen-sink/source/url';
import { useAppStyle } from './App.style';

const CommunicationModule = React.lazy(
  () => import(/* webpackChunkName: "case" */ './modules/communication')
);

const PipCaseDocuments = React.lazy(
  () =>
    import(
      /* webpackChunkName: "PipCaseDocuments" */ './components/PipCaseDocuments/PipCaseDocuments'
    )
);

export interface RoutesPropsType {
  prefix: string;
}

const Routes: React.ComponentType<RoutesPropsType> = ({ prefix }) => {
  const classes = useAppStyle();
  return (
    <Suspense fallback={<Loader delay={200} />}>
      <div className={classes.app}>
        <ErrorBoundary>
          <Switch>
            <Route
              path={`${prefix}/communication/:userUuid`}
              render={({
                match: {
                  url,
                  params: { userUuid },
                },
              }) => {
                const [, queryParam] = window.location.search.split('?');
                const { caseUuid } = objectifyParams(queryParam);

                return (
                  <CommunicationModule
                    userUuid={userUuid}
                    caseUuid={caseUuid}
                    rootPath={url}
                  />
                );
              }}
            />
            <Route
              path={`${prefix}/documents/:caseUUID`}
              render={({
                match: {
                  params: { caseUUID },
                },
              }) => {
                const [, queryParam] = window.location.search.split('?');
                const { case_status } = objectifyParams(queryParam);

                return (
                  <PipCaseDocuments
                    caseUUID={caseUUID}
                    capabilities={{
                      canUpload: case_status === 'open',
                      canDownload: true,
                      canSearch: true,
                    }}
                  />
                );
              }}
            />
          </Switch>
        </ErrorBoundary>
      </div>
    </Suspense>
  );
};

export default Routes;
