// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const queue = [];
let isLoaded = ['complete', 'interactive'].includes(document.readyState);

if (!isLoaded) {
  document.addEventListener('DOMContentLoaded', function onDomContentLoaded() {
    isLoaded = true;

    for (const listener of queue) {
      listener();
    }
  });
}

/**
 * Execute a function when the DOM state is safe for mutations.
 *
 * @param {Function} callback
 */
export default function domReady(callback) {
  if (isLoaded) {
    window.requestAnimationFrame(callback);
  } else {
    queue.push(callback);
  }
}
