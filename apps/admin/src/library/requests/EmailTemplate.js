// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { get } from '@mintlab/kitchen-sink/source';

/**
 * These functions return API response data as a promise,
 * and are used inside the EmailTemplate create/edit screen in the Catalog.
 *
 * API data coming into the form while the user is interacting with it,
 * such as async validation checks or options for a dropdown, are
 * considered temporary, and are not Redux actions.
 */

export const searchDocumentAttributes = search_string =>
  request(
    'GET',
    `/api/v2/admin/catalog/attribute_search?search_string=${search_string}&type=file`
  )
    .catch(() => Promise.reject(false))
    .then(response => get(response, 'data'));
