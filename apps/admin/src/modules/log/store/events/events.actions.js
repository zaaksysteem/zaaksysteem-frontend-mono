// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { getEventsQueryParams } from '../library/log.functions';
import {
  EVENTS_FETCH,
  EVENTS_SET_ROWS_PER_PAGE,
  EVENTS_SET_PAGE,
  EVENTS_EXPORT,
} from './events.contants';

const fetchAjaxAction = createAjaxAction(EVENTS_FETCH);

/**
 * @param {Object} options
 * @param {number} options.page
 * @param {number} options.rowsPerPage
 * @param {number} options.caseNumber
 * @param {string} options.keyword
 * @param {string} options.subject
 * @returns {Promise}
 */
export const eventsFetch = ({
  page = 1,
  rowsPerPage = 50,
  caseNumber,
  keyword,
  user,
} = {}) => {
  const queryParams = getEventsQueryParams({
    page,
    rowsPerPage,
    caseNumber,
    keyword,
    user,
  });
  const url = buildUrl('/api/v1/eventlog', queryParams);

  return fetchAjaxAction({
    url,
    method: 'GET',
    payload: {
      page,
      rowsPerPage,
    },
  });
};

export const eventsSetRowsPerPage = rowsPerPage => ({
  type: EVENTS_SET_ROWS_PER_PAGE,
  payload: {
    rowsPerPage,
  },
});

export const eventsSetPage = page => ({
  type: EVENTS_SET_PAGE,
  payload: {
    page,
  },
});

const exportAjaxAction = createAjaxAction(EVENTS_EXPORT);

export const eventsExport = () => (dispatch, getState) => {
  const {
    log: {
      events: { page, rowsPerPage },
      filters: { caseNumber, keyword, user },
    },
  } = getState();

  const queryParams = getEventsQueryParams({
    page,
    rowsPerPage,
    caseNumber,
    keyword,
    user,
  });

  const url = buildUrl('/api/v1/eventlog/export', queryParams);

  return exportAjaxAction({
    url,
    method: 'GET',
  })(dispatch);
};
