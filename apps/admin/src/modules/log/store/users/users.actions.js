// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { USERS_FETCH_LIST } from './users.constants';

const listAction = createAjaxAction(USERS_FETCH_LIST);

export const fetchUsersList = query =>
  listAction({
    url: buildUrl('/objectsearch/contact/medewerker', {
      query,
      include_admin: '1',
    }),
    method: 'GET',
  });
