// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const CATALOG_SEARCH = 'CATALOG:SEARCH';
export const CATALOG_SEARCH_EXIT = 'CATALOG:SEARCH:EXIT';
