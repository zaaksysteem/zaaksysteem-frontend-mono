// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const CATALOG_FETCH_DETAILS = createAjaxConstants(
  'CATALOG_FETCH_DETAILS'
);
export const CATALOG_TOGGLE_DETAIL_VIEW = 'CATALOG_TOGGLE_DETAIL_VIEW';
