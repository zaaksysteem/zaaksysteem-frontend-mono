// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { ObjectTypeFormShapeType } from '../../components/ObjectTypeForm/ObjectTypeForm.types';
import { createDataFromPayload } from '../../library';
import { OBJECT_TYPE_CREATE } from './create.constants';

export type CreateObjectTypePayloadType = ObjectTypeFormShapeType & {
  folderUuid?: string;
};

const createObjectTypeAjaxAction = createAjaxAction(OBJECT_TYPE_CREATE);

export const createObjectTypeAction = (
  payload: CreateObjectTypePayloadType
) => {
  const data = createDataFromPayload(payload);

  return createObjectTypeAjaxAction({
    url: `/api/v2/cm/custom_object_type/create_custom_object_type`,
    method: 'POST',
    data,
    payload,
  });
};
