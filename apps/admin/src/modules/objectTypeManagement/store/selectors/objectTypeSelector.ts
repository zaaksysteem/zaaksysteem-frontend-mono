// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ObjectTypeManagementRootStateType } from '../objectTypeManagement.reducer';
import { ObjectTypeStateType } from '../objectType/objectType.reducer';

export const objectTypeSelector = (
  state: ObjectTypeManagementRootStateType
): ObjectTypeStateType => state.objectTypeManagement.objectType;
