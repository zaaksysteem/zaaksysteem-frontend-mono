// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @return {JSS}
 */
export const categoriesStyleSheet = () => ({
  menu: {
    padding: '0',
    width: '200px',
  },
});
