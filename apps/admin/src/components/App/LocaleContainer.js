// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { default as locale } from '../../locale/index';
import Locale from './Locale';

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  locale,
});
const LocaleContainer = connect(null, null, mergeProps)(Locale);

export default LocaleContainer;
