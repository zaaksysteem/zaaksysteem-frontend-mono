// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import IconButton from '@material-ui/core/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { DialogsType } from '../../../Case.types';
import { useAboutButtonStyles } from './AboutButton.style';

type CaseActionsButtonPropsType = {
  setDialog: (type: DialogsType) => void;
  folded: boolean;
};

const AboutButton: React.ComponentType<CaseActionsButtonPropsType> = ({
  setDialog,
  folded,
}) => {
  const [t] = useTranslation('case');
  const classes = useAboutButtonStyles();

  const label = t('info.about');
  const action = () => setDialog('about');

  return (
    <Tooltip className={classes.wrapper} title={label} placement="right">
      {folded ? (
        <IconButton className={classes.aboutIcon} onClick={action}>
          <Icon size="small">{iconNames.info}</Icon>
        </IconButton>
      ) : (
        <Button
          className={classes.about}
          action={action}
          presets={['outlined']}
        >
          {label}
        </Button>
      )}
    </Tooltip>
  );
};

export default AboutButton;
