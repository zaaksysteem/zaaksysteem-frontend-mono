// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useRelationsStyles = makeStyles(() => ({
  wrapper: {
    margin: 10,
    display: 'flex',
    flexDirection: 'column',
  },
  section: {
    margin: '10px 0px',
  },
  header: {
    margin: '0 20px',
  },
  actionFooter: {
    width: 400,
    margin: 20,
  },
}));

export type ClassesType = ReturnType<typeof useRelationsStyles>;
