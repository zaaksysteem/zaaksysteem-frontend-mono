// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';

export const fetchPhaseActions = async (caseNumber: number) => {
  const url = `/api/v0/case/${caseNumber}/actions`;

  const response = await request('GET', url);

  return response.result;
};

export const checkPhaseAction = async (
  caseNumber: number,
  id: number,
  automatic: boolean
) => {
  const url = `/zaak/${caseNumber}/action/update`;

  const response = await request('POST', url, { automatic, id });

  return response;
};
