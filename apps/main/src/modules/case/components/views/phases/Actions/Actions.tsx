// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import classNames from 'classnames';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
//@ts-ignore
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { CaseObjType, CaseTypeType } from '../../../../Case.types';
import { useActionsStyles } from './Actions.styles';
import { ActionsType, ActionType } from './Actions.types';
import {
  getActions,
  getTitleTooltip,
  checkAction,
  isActionDisabled,
} from './Actions.library';

export interface ActionsPropsType {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  phaseNumber: number;
  setCheckedActionsCount: (checkedActionsCount: number) => void;
}

const Actions: React.ComponentType<ActionsPropsType> = ({
  caseObj,
  caseType,
  phaseNumber,
  setCheckedActionsCount,
}) => {
  const [t] = useTranslation('caseActions');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [loading, setLoading] = useState<boolean>(true);
  const [actions, setActions] = useState<ActionsType>([]);
  const classes = useActionsStyles();
  const actionsDisabled = !caseObj.canEdit || phaseNumber < caseObj.phase;

  const refreshActions = () => {
    setLoading(true);

    getActions(caseObj, phaseNumber, setActions, setCheckedActionsCount)
      .then(() => {
        setLoading(false);
      })
      .catch(openServerErrorDialog);
  };

  useEffect(() => {
    refreshActions();
  }, [phaseNumber]);

  if (loading) {
    return <Loader />;
  } else if (!actions.length) {
    return <div className={classes.placeholder}>{t('noActions')}</div>;
  } else {
    return (
      <div className={classes.wrapper}>
        {ServerErrorDialog}
        {actions.map((action: ActionType, index: number) => (
          <div
            key={index}
            className={classNames(classes.action, {
              [classes.actionActive]: !actionsDisabled,
            })}
          >
            <Tooltip
              className={classes.checkboxWrapper}
              title={t('checkboxTooltip')}
            >
              <Checkbox
                name="phase-action-automatic"
                onChange={() => {
                  checkAction(
                    caseObj.number,
                    action,
                    actions,
                    setActions,
                    setCheckedActionsCount
                  ).catch(openServerErrorDialog);
                }}
                checked={action.automatic}
                disabled={
                  actionsDisabled ||
                  isActionDisabled(action, caseType, phaseNumber)
                }
              />
            </Tooltip>
            <Tooltip
              className={classNames(classes.titleWrapper, {
                [classes.titleWrapperDisabled]: actionsDisabled,
              })}
              title={getTitleTooltip(t, action)}
            >
              <>
                {action.icon}
                <span className={classes.title}>{action.label}</span>
              </>
            </Tooltip>
          </div>
        ))}
      </div>
    );
  }
};

export default Actions;
