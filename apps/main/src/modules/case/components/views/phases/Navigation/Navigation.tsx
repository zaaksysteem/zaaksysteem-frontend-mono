// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Link } from 'react-router-dom';
import Counter from '@mintlab/ui/App/Zaaksysteem/Counter/Counter';
import Icon from '@mintlab/ui/App/Material/Icon';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { SideMenuItemType } from '@mintlab/ui/App/Zaaksysteem/SideMenu';
import { useNavigationStyles } from './Navigation.styles';

const MenuItemLink = React.forwardRef<any, SideMenuItemType>(
  ({ href, ...restProps }, ref) => (
    <Link innerRef={ref} to={href || ''} {...restProps} />
  )
);

type NavigationPropsType = {
  items: {
    href: string;
    selected: boolean;
    icon: string;
    label: string;
    fullWidth?: boolean;
  }[];
};

export const Navigation: React.ComponentType<NavigationPropsType> = ({
  items,
}) => {
  const classes = useNavigationStyles();

  return (
    <List className={classes.wrapper} component="nav" role="navigation">
      {items.map(
        (
          { href, selected, icon, label, count, fullWidth = true }: any,
          index: number
        ) => (
          <ListItem
            button
            role="button"
            href={href}
            component={MenuItemLink}
            key={index}
            selected={selected}
            classes={{
              root: fullWidth ? classes.item : classes.itemSmall,
              selected: classes.itemSelected,
            }}
          >
            {icon && (
              <ListItemIcon
                classes={{
                  root: selected ? classes.iconSelected : classes.icon,
                }}
              >
                <Icon size="small">{icon}</Icon>
              </ListItemIcon>
            )}
            <ListItemText classes={{ root: classes.itemLabel }}>
              <Counter count={count}>{label}</Counter>
            </ListItemText>
          </ListItem>
        )
      )}
    </List>
  );
};

export default Navigation;
