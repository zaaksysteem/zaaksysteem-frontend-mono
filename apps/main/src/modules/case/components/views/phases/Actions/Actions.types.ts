// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type ActionCaseRelationType =
  | 'deelzaak'
  | 'gerelateerd'
  | 'vervolgzaak_datum'
  | 'vervolgzaak';

type ActionDataCaseType = {
  relatie_type: ActionCaseRelationType;
  [key: string]: any;
};
type ActionDataTemplateType = {
  data: {
    interface_id?: number;
  };
  [key: string]: any;
};
type ActionDataEmailType = any;
type ActionDataObjectActionType = any;
type ActionDataAllocationType = any;

type ActionDataType =
  | ActionDataCaseType
  | ActionDataTemplateType
  | ActionDataEmailType
  | ActionDataObjectActionType
  | ActionDataAllocationType;

export type ActionTypeType =
  | 'case'
  | 'template'
  | 'email'
  | 'object_action'
  | 'allocation';
export type ActionType = {
  id: number;
  automatic: boolean;
  description: string;
  label: string;
  tainted: boolean;
  type: ActionTypeType;
  data: ActionDataType;
  icon: React.ComponentType;
};
export type ActionsType = ActionType[];
export type ActionsOfPhaseType = {
  milestone: number;
  items: ActionsType;
};
export type ActionsByPhaseType = ActionsOfPhaseType[];
