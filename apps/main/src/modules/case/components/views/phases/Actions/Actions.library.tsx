// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import { CaseObjType, CaseTypeType } from '../../../../Case.types';
import { ActionsType, ActionType, ActionsOfPhaseType } from './Actions.types';
import { fetchPhaseActions, checkPhaseAction } from './Actions.requests';
import Icons from './icons/Icons';

const getActionIcon = (action: ActionType) => {
  const ActionIcon = Icons[action.type];

  return <ActionIcon />;
};

const areActionsOfPhase =
  (phaseNumber: number) => (actionsOfPhase: ActionsOfPhaseType) =>
    actionsOfPhase.milestone === phaseNumber;

const supportedActionTypes = [
  'allocation',
  'case',
  'template',
  'email',
  'object_action',
  // 'object_mutation,
];

const isSupportedType = (action: ActionType) =>
  supportedActionTypes.includes(action.type);

const formatActions = (actions: any[]): ActionsType =>
  actions.map(action => ({
    ...action,
    checked: true,
    icon: getActionIcon(action),
  }));

const countCheckedActions = (actions: ActionType[]): number =>
  actions.filter((action: ActionType) => action.automatic).length;

export const getActions = async (
  caseObj: CaseObjType,
  phaseNumber: number,
  setActions: (actions: ActionsType) => void,
  setCheckedActionsCount: (checkedActionsCount: number) => void
) => {
  const actionsByPhase = await fetchPhaseActions(caseObj.number);
  const actions = actionsByPhase.find(areActionsOfPhase(phaseNumber)).items;
  const supportedActions = actions.filter(isSupportedType);
  const formattedActions = formatActions(supportedActions);

  const checkedActionsCount = countCheckedActions(formattedActions);

  setCheckedActionsCount(checkedActionsCount);
  setActions(formattedActions);
};

export const getTitleTooltip = (t: i18next.TFunction, action: ActionType) => {
  let noun = t(`${action.type}.noun`);
  const verb = t(`${action.type}.verb`);

  if (action.type === 'case') {
    noun = t(`case.noun.${action.data.relatie_type}`);
  }

  return t('titleTooltip', {
    noun,
    verb,
  });
};

export const isActionDisabled = (
  action: ActionType,
  caseType: CaseTypeType,
  phaseNumber: number
): boolean => {
  const hasInterface = Boolean(action.data.interface_id); // e.g. xential
  const isSubcaseInLastPhase =
    action.data.relatie_type === 'deelzaak' &&
    caseType.phases.length === phaseNumber;

  return hasInterface || isSubcaseInLastPhase;
};

export const checkAction = async (
  caseNumber: number,
  action: ActionType,
  actions: ActionsType,
  setActions: (actions: ActionsType) => void,
  setCheckedActionsCount: (checkedActionsCount: number) => void
) => {
  const updatedActions = actions.reduce((acc: ActionsType, act: ActionType) => {
    if (act.id === action.id) {
      return [...acc, { ...action, automatic: !action.automatic }];
    }

    return [...acc, act];
  }, []);

  const checkedActionsCount = countCheckedActions(updatedActions);

  setActions(updatedActions);
  setCheckedActionsCount(checkedActionsCount);

  checkPhaseAction(caseNumber, action.id, !action.automatic).catch(() => {
    const previouslyCheckedActionsCount = countCheckedActions(actions);

    setActions(actions);
    setCheckedActionsCount(previouslyCheckedActionsCount);
  });
};
