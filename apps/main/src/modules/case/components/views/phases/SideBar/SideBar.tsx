// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Route } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { CaseObjType, CaseTypeType } from '../../../../Case.types';
import Navigation from './../Navigation/Navigation';
import { useSideBarStyles } from './SideBar.styles';
import { getSideBarItems } from './../Phases.library';
import Tasks from './../Tasks';
import Actions from './../Actions';

export interface SideBarPropsType {
  rootPath: string;
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  phaseNumber: number;
  checkedActionsCount?: number;
  setCheckedActionsCount: (checkedActionsCount: number) => void;
  openTasksCount?: number;
  setOpenTasksCount: (openTasksCount: number) => void;
}

export const SideBar: React.ComponentType<SideBarPropsType> = ({
  rootPath,
  caseObj,
  caseType,
  phaseNumber,
  checkedActionsCount,
  setCheckedActionsCount,
  openTasksCount,
  setOpenTasksCount,
}) => {
  const [t] = useTranslation('casePhases');
  const classes = useSideBarStyles();
  const defaultBar = 'actions';
  const activeBar = window.location.pathname.split('/')[6] || defaultBar;

  return (
    <div className={classes.wrapper}>
      <Navigation
        items={getSideBarItems(
          t,
          rootPath,
          activeBar,
          phaseNumber,
          checkedActionsCount,
          openTasksCount
        )}
      />
      <Route
        path={`${rootPath}/:type(actions|tasks)?`}
        render={({ match }: any) => {
          const type = match.params.type || 'actions';
          const url = `${rootPath}/${type}`;

          return (
            <>
              <div
                className={classes.content}
                style={{ display: type === 'actions' ? 'block' : 'none' }}
              >
                <Actions
                  caseObj={caseObj}
                  caseType={caseType}
                  phaseNumber={phaseNumber}
                  setCheckedActionsCount={setCheckedActionsCount}
                />
              </div>
              <div
                className={classes.content}
                style={{ display: type === 'tasks' ? 'block' : 'none' }}
              >
                <Tasks
                  rootPath={url}
                  caseUuid={caseObj.uuid}
                  phaseNumber={phaseNumber}
                  setOpenTasksCount={setOpenTasksCount}
                />
              </div>
            </>
          );
        }}
      />
    </div>
  );
};

export default SideBar;
