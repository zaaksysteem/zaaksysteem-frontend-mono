// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { Route, Switch } from 'react-router-dom';
import {
  deleteTaskAction,
  getTasks,
  setTaskCompletionAction,
  updateTaskAction,
} from './Tasks.library';
import {
  TasksType,
  HandleChangeType,
  AddTaskType,
  UpdateTaskType,
  DeleteTaskType,
  SetTaskCompletionType,
  TaskType,
} from './Tasks.types';
import { TaskList } from './components/TaskList/TaskList';
import Details from './components/Details/Details';
import { addTaskAction, countOpenTasks } from './Tasks.library';

export interface TasksPropsType {
  rootPath: string;
  caseUuid: string;
  phaseNumber: number;
  setOpenTasksCount: (checkedActionsCount: number) => void;
  // iframe support
  iframe?: boolean;
  // ---
}

const Tasks: React.ComponentType<TasksPropsType> = ({
  rootPath,
  caseUuid,
  phaseNumber,
  setOpenTasksCount,
  // iframe support
  iframe,
  // ---
}) => {
  const history = useHistory();
  const [tasks, setTasks] = useState<TasksType>();
  // iframe support
  const [taskToEdit, setTaskToEdit] = useState<TaskType>();
  // ---
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  useEffect(() => {
    getTasks(caseUuid, phaseNumber, setTasks, setOpenTasksCount);
  }, [phaseNumber]);

  if (!tasks) {
    return <Loader />;
  }

  const enterEditMode = (task: TaskType) => {
    history.push(`${rootPath}/edit/${task.task_uuid}`);
    // iframe support
    setTaskToEdit(task);
    // ---
  };

  const exitEditMode = () => {
    history.push(rootPath);
    // iframe support
    setTaskToEdit(undefined);
    // ---
  };

  const handleChange: HandleChangeType = (action, tasks, newTasks) => {
    const newOpenTasks = countOpenTasks(newTasks);

    setTasks(newTasks);

    // iframe support
    if (!iframe) {
      setOpenTasksCount(newOpenTasks);
    }
    // ---

    exitEditMode();

    action()
      // iframe support
      .then(() => {
        if (iframe) {
          // refresh
          setOpenTasksCount(0);
        }
      })
      // ---
      .catch((err: any) => {
        const openTasks = countOpenTasks(tasks);

        setOpenTasksCount(openTasks);
        setTasks(tasks);
        openServerErrorDialog(err);
      });
  };

  const addTask: AddTaskType = title => {
    addTaskAction(caseUuid, phaseNumber, title, tasks, handleChange);
  };

  const updateTask: UpdateTaskType = (task_uuid, values) => {
    updateTaskAction(task_uuid, values, tasks, handleChange);
  };

  const deleteTask: DeleteTaskType = task_uuid => {
    deleteTaskAction(task_uuid, tasks, handleChange);
  };

  const setTaskCompletion: SetTaskCompletionType = (task_uuid, completed) => {
    setTaskCompletionAction(task_uuid, completed, tasks, handleChange);
  };

  // iframe support
  if (taskToEdit) {
    return (
      <Details
        task={taskToEdit}
        updateTask={updateTask}
        deleteTask={deleteTask}
        setTaskCompletion={setTaskCompletion}
        exitEditMode={exitEditMode}
      />
    );
  }
  // ---

  return (
    <>
      {ServerErrorDialog}
      <Switch>
        <Route
          exact
          path={rootPath}
          render={() => (
            <TaskList
              rootPath={rootPath}
              tasks={tasks}
              addTask={addTask}
              setTaskCompletion={setTaskCompletion}
              enterEditMode={enterEditMode}
            />
          )}
        />
        <Route
          path={`${rootPath}/edit/:task_uuid`}
          render={({
            match: {
              params: { task_uuid },
            },
          }) => {
            const task = tasks.find(task => task.task_uuid === task_uuid);

            if (!task) {
              exitEditMode();
              return;
            }

            return (
              <Details
                task={task}
                updateTask={updateTask}
                deleteTask={deleteTask}
                setTaskCompletion={setTaskCompletion}
                exitEditMode={exitEditMode}
              />
            );
          }}
        />
      </Switch>
    </>
  );
};

export default Tasks;
