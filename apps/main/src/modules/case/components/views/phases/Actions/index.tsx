// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Actions, { ActionsPropsType } from './Actions';
import locale from './Actions.locale';

const ActionsView: React.ComponentType<ActionsPropsType> = props => (
  <I18nResourceBundle resource={locale} namespace="caseActions">
    <Actions {...props} />
  </I18nResourceBundle>
);

export default ActionsView;
