// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import fecha from 'fecha';
import { SessionType } from '@zaaksysteem/common/src/store/session/session.reducer';
import { NotificationType } from '../../components/NotificationBar/NotificationBar';
import {
  fetchCase,
  fetchCaseV0,
  fetchCaseV1,
  fetchCaseType,
  fetchCaseTypeV1,
  fetchSubject,
  setPaymentStatus,
  assignToSelf,
  fetchJobs,
} from './Case.requests';
import {
  CaseV2Type,
  CaseV1Type,
  CaseTypeType,
  CaseTypeV2Type,
  CaseTypeV1Type,
  CaseObjType,
  SystemRolesType,
  SubjectV2Type,
  SubjectType,
  JobTypeV1,
  JobType,
  DialogsType,
  CaseActionDialogType,
} from './Case.types';
import { caseActions } from './components/sideMenu/caseActions/Caseactions.library';

export const formatBreadcrumbLabel = (
  t: i18next.TFunction,
  caseObj: CaseObjType,
  caseType: CaseTypeType
): string =>
  `${t('common:entityType.case')} ${caseObj.number}: ${caseType.name}`;

export const formatSubject = (subjectV2: SubjectV2Type): SubjectType => {
  const {
    id,
    meta,
    attributes: {
      status,
      coc_number,
      coc_location_number,
      is_secret,
      correspondence_address,
      date_of_death,
      investigation,
    },
    type,
  } = subjectV2.data;

  return {
    type,
    uuid: id,
    name: meta?.summary,
    status: status,
    cocNumber: coc_number,
    cocLocationNumber: coc_location_number,
    isSecret: is_secret,
    correspondenceAdress: correspondence_address,
    dateOfDeath: date_of_death,
    investigation: investigation,
  };
};

// recipient is a caseRole, but is not included in the get_case call as one
// it's instead included as a regular related contact (legacy mistake)
const getSystemRoles = async (caseV2: CaseV2Type) => {
  const relationships = caseV2.data.relationships;

  // a case will always have a requestor, and thus relationships
  // but the apidocs don't know that
  if (!relationships) return;

  const caseRoles = ['assignee', 'coordinator', 'requestor' /*, 'recipient' */];
  const subjectPromises = caseRoles.map(async (caseRole: string) => {
    const existsInCase = Object.prototype.hasOwnProperty.call(
      relationships,
      caseRole
    );

    if (!existsInCase) return;

    const subjectRelationship = relationships[caseRole];
    const type = subjectRelationship.data.type;

    return fetchSubject(type, subjectRelationship.data.id);
  });

  const relatedContacts = relationships.related_contacts;
  const recipientData = (relatedContacts || []).find(
    contact => contact.meta?.role === 'Ontvanger'
  );

  const recipientPromises = recipientData
    ? // everything is optional according to the apidocs, but that's completely unworkable
      // @ts-ignore
      [fetchSubject(recipientData.data.type, recipientData.data.id)]
    : [];

  const systemRoles = await Promise.all([
    ...subjectPromises,
    ...recipientPromises,
  ]);

  const [assignee, coordinator, requestor, recipient] = systemRoles.map(
    subjectV2 => {
      if (!subjectV2) return;

      return formatSubject(subjectV2);
    }
  );

  return {
    assignee,
    requestor,
    recipient,
    coordinator,
  };
};

export const formatCaseV1 = (caseV1: CaseV1Type) => ({
  // v2 doesn't return the html_email_template (for communication tab)
  htmlEmailTemplateName: caseV1.html_email_template,
  // map fields require the v1 response to be passed into the context (for Wetterskip)
  caseV1,
  // needed to fetch caseTypeV1
  caseTypeUuid: caseV1.casetype.reference,
  caseTypeVersion: caseV1.casetype.instance.version,
});

export const formatCaseV2 = (caseV2: CaseV2Type) => {
  const data = caseV2.data;
  const id = data.id;
  const summary = data.meta?.summary || '';
  const authorizations = data.meta?.authorizations;
  const case_type_version_uuid = data.attributes?.case_type_version_uuid;
  const confidentiality = data.attributes?.confidentiality;
  const contact_channel = data.attributes?.contact_channel;
  const custom_fields = data.attributes?.custom_fields;
  const department = data.attributes?.department;
  const milestone = data.attributes?.milestone;
  const number = data.attributes?.number;
  const payment = data.attributes?.payment;
  const preset_client = data.attributes?.preset_client;
  const progress_status = data.attributes?.progress_status;
  const result = data.attributes?.result;
  const resultDescription = data.attributes?.result_description;
  const role = data.attributes?.role;
  const stalled_since_date = data.attributes?.stalled_since_date;
  const stalled_until_date = data.attributes?.stalled_until_date;
  const status = data.attributes?.status;
  const registration_date = data.attributes?.registration_date;
  const target_completion_date = data.attributes?.target_completion_date;
  const completion_date = data.attributes?.completion_date;
  const destruction_date = data.attributes?.destruction_date;

  // most actions will only be available when 1. the case is open and 2. for those with edit/manage rights
  // so we pass canEdit/canManage for convenience, and pass the rest to be used for the exceptions
  // @ts-ignore apidocs
  const caseOpen = status !== 'resolved';
  const hasEditRights = authorizations.includes('write');
  const hasManageRights = authorizations.includes('manage');
  const canEdit = hasEditRights && caseOpen;
  const canManage = hasManageRights && caseOpen;

  const presetClient = preset_client === 'True' ? true : false;
  const phase = caseOpen ? milestone + 1 : milestone;

  return {
    caseOpen,
    hasEditRights,
    hasManageRights,
    canEdit,
    canManage,

    registrationDate: registration_date,
    targetDate: target_completion_date,
    completionDate: completion_date,
    destructionDate: destruction_date,

    uuid: id,
    name: id,
    number: number,
    status: status,
    department: department.name,
    role: role.name,
    milestone,
    phase,
    summary,
    result: result?.result,
    resultDescription,

    caseTypeVersionUuid: case_type_version_uuid,
    confidentiality: confidentiality,
    contactChannel: contact_channel,
    customFields: custom_fields,
    // not supported by v2, or even v1
    location: undefined,
    payment: payment,
    presetClient,
    progress_status: progress_status / 100,
    stalledSinceDate: stalled_since_date,
    stalledUntilDate: stalled_until_date,
  };
};

const buildCaseObj = (
  caseV2: CaseV2Type,
  caseV1?: CaseV1Type,
  systemRoles?: SystemRolesType
  // everything is optional according to the apidocs, but that's completely unworkable
  // @ts-ignore
): CaseObjType => ({
  ...formatCaseV1(caseV1),
  ...formatCaseV2(caseV2),
  ...systemRoles,
});

const getFields = (caseTypeV1: CaseTypeV1Type) => {
  const phases = caseTypeV1.phases;
  const fields = phases.reduce(
    (acc: any[], phase: any) => [...acc, ...phase.fields],
    []
  );

  return fields;
};

const getObjectFields = (caseTypeV1: CaseTypeV1Type) => {
  const fields = getFields(caseTypeV1);
  const objectFields = fields.filter((field: any) => field.type === 'object');

  return objectFields;
};

export const formatCaseTypeV1 = (caseTypeV1: CaseTypeV1Type) => ({
  // v2 lacks object(v1) attributes in its phase definition (for relations tab)
  objectFields: getObjectFields(caseTypeV1),
  // v2 lacks the 'id' of an attribute in its field definition (for object form)
  fields: getFields(caseTypeV1),
  // v2 doesn't return the results (for caseAction 'resolve prematurely')
  results: caseTypeV1.results,
});

export const formatCaseTypeV2 = (caseTypeV2: CaseTypeV2Type) => ({
  phases: caseTypeV2.data?.attributes.phases,
  settings: caseTypeV2.data?.attributes.settings,
  name: caseTypeV2.data?.meta.summary,
  metaData: {
    legalBasis: caseTypeV2.data?.attributes.metadata.legal_basis,
    localBasis: caseTypeV2.data?.attributes.metadata.local_basis,
    designationOfConfidentiality:
      caseTypeV2.data?.attributes.metadata.designation_of_confidentiality,
    responsibleRelationship:
      caseTypeV2.data?.attributes.metadata.responsible_relationship,
    processDescription:
      caseTypeV2.data?.attributes.metadata.process_description,
  },
  leadTimeLegal: {
    type: caseTypeV2.data?.attributes.terms.lead_time_legal.type,
    value: caseTypeV2.data?.attributes.terms.lead_time_legal.value,
  },
  leadTimeService: {
    type: caseTypeV2.data?.attributes.terms.lead_time_service.type,
    value: caseTypeV2.data?.attributes.terms.lead_time_service.value,
  },
});

const buildCaseType = (
  caseTypeV2: CaseTypeV2Type,
  caseTypeV1: CaseTypeV1Type
): CaseTypeType => ({
  ...formatCaseTypeV1(caseTypeV1),
  ...formatCaseTypeV2(caseTypeV2),
});

export const getCaseObj = async (
  caseUuid: string,
  setCaseObj: (caseObj: CaseObjType) => void
) => {
  const [caseV2, caseV1] = await Promise.all([
    fetchCase(caseUuid),
    fetchCaseV1(caseUuid),
  ]);

  const systemRoles = await getSystemRoles(caseV2);

  const caseObj = buildCaseObj(caseV2, caseV1, systemRoles);

  setCaseObj(caseObj);
};

export const getCaseType = async (
  caseObj: CaseObjType,
  setCaseType: (caseType: CaseTypeType) => void
) => {
  const caseTypeVersionUuid = caseObj.caseTypeVersionUuid;
  const caseTypeUuid = caseObj.caseTypeUuid;
  const caseTypeVersion = caseObj.caseTypeVersion;

  const [caseTypeV2, caseTypeV1] = await Promise.all([
    fetchCaseType(caseTypeVersionUuid),
    fetchCaseTypeV1(caseTypeUuid, caseTypeVersion),
  ]);

  const caseType = buildCaseType(caseTypeV2, caseTypeV1);

  setCaseType(caseType);
};

export const getJobs = async (
  caseUuid: string,
  setJobs: (jobs: any) => void
) => {
  const response = await fetchJobs(caseUuid);

  const jobs = response.result.instance.rows.map((job: JobTypeV1) => ({
    status: job.instance.status,
  }));

  setJobs(jobs);
};

export const redirectToCaseUuid = async (caseNumber: string) => {
  const caseV0 = await fetchCaseV0(caseNumber);
  const caseUuid = caseV0.id;

  window.location.href = `/main/case/${caseUuid}`;
};

export const provideForCase = async (caseUuid: string) => {
  const [caseV2, caseV1] = await Promise.all([
    fetchCase(caseUuid),
    fetchCaseV1(caseUuid),
  ]);

  const systemRoles = await getSystemRoles(caseV2);

  const caseTypeVersionUuid = caseV2.data.attributes?.case_type_version_uuid;
  const caseTypeUuid = caseV1.casetype.reference;
  const caseTypeVersion = caseV1.casetype.instance.version;

  const [caseTypeV2, caseTypeV1] = await Promise.all([
    fetchCaseType(caseTypeVersionUuid),
    fetchCaseTypeV1(caseTypeUuid, caseTypeVersion),
  ]);

  const caseType = buildCaseType(caseTypeV2, caseTypeV1);

  const caseObj = buildCaseObj(caseV2, caseV1, systemRoles);

  return {
    caseObj,
    caseType,
  };
};

export const formatDate = (date: string | null): string | undefined => {
  if (!date) return;

  return fecha.format(new Date(date), 'DD-MM-YYYY');
};

const finishedStatuses = ['finished', 'failed', 'cancelled'];

export const getUnfinishedJobs = (jobs: JobType[]) =>
  jobs.filter(({ status }) => !finishedStatuses.includes(status));

/* eslint complexity: [2, 10] */
export const getNotifications = (
  t: i18next.TFunction,
  session: SessionType,
  caseObj: CaseObjType,
  refreshCaseObj: () => void,
  jobs: JobType[],
  setSnackOpen: (open: boolean) => void
): NotificationType[] => {
  const paymentStatus = caseObj.payment.status;
  const status = caseObj.status;

  const statusNotifications = [
    {
      when: status === 'stalled',
      message: caseObj.stalledUntilDate
        ? t('notification.status.stalled.definite', {
            date: formatDate(caseObj.stalledUntilDate),
          })
        : t('notification.status.stalled.indefinite'),
    },
    {
      when: status === 'resolved',
      message: t('notification.status.closed'),
    },
  ];

  const assignee = caseObj.assignee;
  const userIsAssignee = session.logged_in_user.uuid === assignee?.uuid;
  const assignCaseToSelf = () =>
    assignToSelf(caseObj.uuid).then(() => refreshCaseObj());
  const assigneeNotifications = [
    {
      // this action is available to those with read rights
      when: !caseObj.assignee && status !== 'resolved',
      message: t('notification.assignment.assignToSelf'),
      button: {
        label: t('notification.actions.assignToSelf'),
        action: assignCaseToSelf,
      },
    },
    {
      // this action is available to those with read rights
      when: status === 'new' && assignee && userIsAssignee,
      message: t('notification.assignment.acceptAssignment'),
      button: {
        label: t('notification.actions.assignToSelf'),
        action: assignCaseToSelf,
      },
    },
    {
      when: status !== 'resolved' && assignee && !userIsAssignee,
      message: t('notification.assignment.userIsNotAssignee'),
    },
  ];

  const requestor = caseObj.requestor;
  const requestorNotifications = [
    {
      when: Boolean(requestor?.correspondenceAdress),
      message: t('notification.requestor.correspondenceAddress'),
    },
    {
      when: Boolean(requestor?.dateOfDeath),
      message: t('notification.requestor.deceased'),
    },
    {
      when: requestor?.investigation === 'In onderzoek',
      message: t('notification.requestor.investigated'),
    },
    {
      when: requestor?.status === 'Verhuisd',
      message: t('notification.requestor.moved'),
    },
    {
      when: requestor?.isSecret,
      message: t('notification.requestor.secret'),
    },
  ];

  const paymentNotifications = [
    {
      when: paymentStatus === 'failed',
      message: t(`notification.payment.failed`),
    },
    {
      when: paymentStatus === 'offline' && status !== 'resolved',
      message: t(`notification.payment.${paymentStatus}`),
      button: {
        label: t('notification.actions.payment'),
        disabled: !caseObj.canEdit,
        action: () => {
          setPaymentStatus(caseObj.uuid, 'success').then(() =>
            refreshCaseObj()
          );
        },
      },
    },
    {
      when:
        paymentStatus === 'pending' ||
        (paymentStatus === 'offline' && status === 'resolved'),
      message: t(`notification.payment.pending`),
    },
  ];

  const unfinishedJobs = getUnfinishedJobs(jobs);
  const jobNotifications = [
    {
      when: unfinishedJobs.length > 0,
      message: t('notification.jobs.unfinished'),
      button: {
        label: t('notification.actions.seeJobs'),
        action: () => {
          setSnackOpen(true);

          const interval = setInterval(() => {
            refreshCaseObj();

            if (unfinishedJobs.length === 0) {
              clearInterval(interval);
            }
          }, 5000);
        },
      },
    },
  ];

  return [
    ...statusNotifications,
    ...assigneeNotifications,
    ...requestorNotifications,
    ...paymentNotifications,
    ...jobNotifications,
  ]
    .filter(notification => notification.when)
    .map(({ message, button }: any) => ({
      message,
      button,
    }));
};

export const dialogIsCaseAction = (
  dialog: DialogsType
): dialog is CaseActionDialogType =>
  caseActions.some(action => action === dialog);
