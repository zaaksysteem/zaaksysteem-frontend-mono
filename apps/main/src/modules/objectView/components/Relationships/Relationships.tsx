// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { H2 } from '@mintlab/ui/App/Material/Typography';
import {
  ObjectType,
  RelatedCaseRowType,
  RelatedObjectRowType,
  RelatedSubjectRowType,
} from '../../ObjectView.types';
import {
  getCaseRelations,
  getObjectRelations,
  getSubjectRelations,
} from './../../ObjectView.library';
import { useRelationshipsStyles } from './Relationships.styles';
import RelatedObjects from './components/RelatedObjects';
import RelatedCases from './components/RelatedCases';
import RelatedSubjects from './components/RelatedSubjects';

type RelationshipsPropsType = {
  object: ObjectType;
};

const Relationships: React.FunctionComponent<RelationshipsPropsType> = ({
  object: { versionIndependentUuid },
}) => {
  const classes = useRelationshipsStyles();
  const [t] = useTranslation('main');
  const [caseRelations, setCaseRelations] = useState<RelatedCaseRowType[]>();
  const [objectRelations, setObjectRelations] =
    useState<RelatedObjectRowType[]>();
  const [subjectRelations, setSubjectRelations] =
    useState<RelatedSubjectRowType[]>();

  useEffect(() => {
    getCaseRelations(versionIndependentUuid, setCaseRelations);
    getObjectRelations(versionIndependentUuid, setObjectRelations);
    getSubjectRelations(versionIndependentUuid, setSubjectRelations);
  }, []);

  if (!caseRelations || !objectRelations || !subjectRelations) {
    return <Loader />;
  }

  return (
    <div className={classes.wrapper}>
      <H2
        classes={{
          root: classes.mainTitle,
        }}
      >
        {t('objectView:relationships.title')}
      </H2>

      <RelatedCases cases={caseRelations} />
      <RelatedObjects objects={objectRelations} />
      <RelatedSubjects subjects={subjectRelations} />
    </div>
  );
};

export default Relationships;
