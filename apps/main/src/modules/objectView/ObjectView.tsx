// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Route, Switch, Redirect } from 'react-router-dom';
import { PanelLayout } from '../../components/PanelLayout/PanelLayout';
import { Panel } from '../../components/PanelLayout/Panel';
import BreadcrumbBar, {
  BreadcrumbBarPropsType,
} from '../../components/BreadcrumbBar/BreadcrumbBar';
import { createPath } from '../../library/createPath';
import SideMenu from './components/Menu/Menu';
import Map from './components/Map/Map';
import { useObjectViewStyles } from './ObjectView.style';
import Attributes from './components/Attributes/Attributes';
import Relationships from './components/Relationships/Relationships';
import Timeline from './components/Timeline';
import { ObjectType, ObjectTypeType } from './ObjectView.types';

export interface ObjectViewPropsType {
  rootPath: string;
  object: ObjectType;
  objectType: ObjectTypeType;
  refreshObject: () => void;
}

const ObjectView: React.FunctionComponent<ObjectViewPropsType> = ({
  rootPath,
  object,
  objectType,
  refreshObject,
}) => {
  const classes = useObjectViewStyles();
  const [t] = useTranslation('main');

  const createPathObject = createPath(rootPath);

  const breadcrumbs: BreadcrumbBarPropsType['breadcrumbs'] = [
    {
      label: t('modules.dashboard'),
      path: '/intern',
    },
    {
      label: object.title,
      path: createPathObject(),
    },
  ];

  return (
    <div className={classes.wrapper}>
      <BreadcrumbBar breadcrumbs={breadcrumbs} />
      <div className={classes.content}>
        <PanelLayout>
          <Panel type="side">
            <SideMenu rootPath={rootPath} />
          </Panel>
          <Panel>
            <Switch>
              <Redirect exact from={rootPath} to={`${rootPath}/attributes`} />
              <Route
                path={`${rootPath}/attributes`}
                render={() => (
                  <Attributes
                    object={object}
                    objectType={objectType}
                    refreshObject={refreshObject}
                  />
                )}
              />
              <Route
                exact
                path={`${rootPath}/timeline`}
                render={() => <Timeline uuid={object.versionIndependentUuid} />}
              />
              <Route
                exact
                path={`${rootPath}/map`}
                render={() => (
                  <Map
                    object={object}
                    objectType={objectType}
                    objectUuid={object.versionIndependentUuid}
                  />
                )}
              />
              <Route
                exact
                path={`${rootPath}/relationships`}
                render={() => <Relationships object={object} />}
              />
            </Switch>
          </Panel>
        </PanelLayout>
      </div>
    </div>
  );
};

export default ObjectView;
