// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import locale from './ObjectView.locale';
import ObjectView from './ObjectView';
import { ObjectType, ObjectTypeType } from './ObjectView.types';
import { getObject, getObjectType } from './ObjectView.library';

const ObjectViewModule: React.ComponentType<{
  rootPath: string;
  uuid: string;
}> = ({ rootPath, uuid }) => {
  const [object, setObject] = useState<ObjectType>();
  const [objectType, setObjectType] = useState<ObjectTypeType>();

  useEffect(() => {
    if (!object) {
      getObject(uuid, setObject);
    }

    if (object && !objectType) {
      getObjectType(object, setObjectType);
    }
  }, [object]);

  if (!object || !objectType) {
    return <Loader />;
  }

  const refreshObject = () => getObject(uuid, setObject);

  return (
    <I18nResourceBundle resource={locale} namespace="objectView">
      <ObjectView
        rootPath={rootPath}
        object={object}
        objectType={objectType}
        refreshObject={refreshObject}
      />
    </I18nResourceBundle>
  );
};

export default ObjectViewModule;
