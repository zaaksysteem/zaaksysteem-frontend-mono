// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import Button from '@mintlab/ui/App/Material/Button';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { useInformationStyles } from '../../Information.style';
import { savePhoneExtension } from '../../requests';
import { getFormDefinition } from './phoneExtension.formDefinition';
import { SubjectType } from './../../../ContactView.types';

type SignaturePropsType = {
  subject: SubjectType;
};

const PhoneExtension: React.FunctionComponent<SignaturePropsType> = ({
  subject,
}) => {
  const [t] = useTranslation('contactView');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const classes = useInformationStyles();
  const formDefinition = getFormDefinition(t, subject);

  const {
    fields,
    formik: { values },
  } = useForm({
    formDefinition,
  });

  return (
    <>
      {ServerErrorDialog}
      <SubHeader
        title={t('phoneExtension.title')}
        description={t('phoneExtension.subTitle')}
      />
      <div className={classes.formWrapper}>
        {fields.map(
          ({ FieldComponent, name, error, touched, value, ...rest }) => {
            const restValues = {
              ...cloneWithout(rest, 'type', 'classes'),
              disabled: values.completed,
            };

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
              >
                <FieldComponent
                  name={name}
                  value={value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}
        <Button
          action={() =>
            savePhoneExtension(subject.uuid, values.phoneExtension).catch(
              openServerErrorDialog
            )
          }
          className={classes.button}
          presets={['contained', 'primary']}
        >
          {t('save')}
        </Button>
      </div>
    </>
  );
};

export default PhoneExtension;
