// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const usePanelLayoutStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flex: 1,
      height: '100%',
    },
    panel: {
      borderRight: `1px solid ${greyscale.dark}`,
      overflow: 'auto',

      '&:last-child': {
        border: 'none',
      },
    },
    fluid: {
      flex: 1,
    },
    side: {
      minWidth: 244,
    },
  })
);
