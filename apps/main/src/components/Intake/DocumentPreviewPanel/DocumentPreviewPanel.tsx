// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import { DocumentPreview } from '@zaaksysteem/common/src/components/DocumentPreview';
import { useIntakeStyles } from '../Intake.styles';

type DocumentPreviewPanelPropsType = {
  url?: string;
  contentType?: string;
  t: i18next.TFunction;
};

const DocumentPreviewPanel: React.ComponentType<
  DocumentPreviewPanelPropsType
> = ({ url, contentType, t }) => {
  const intakeClasses = useIntakeStyles();
  return (
    <div className={intakeClasses.documentPreview}>
      {url ? (
        <DocumentPreview url={url} contentType={contentType} />
      ) : (
        <p className={intakeClasses.documentPreviewNoSelection}>
          {t('Intake:previewPanel')}
        </p>
      )}
    </div>
  );
};

export default DocumentPreviewPanel;
