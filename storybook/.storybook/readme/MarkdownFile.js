import React from 'react';
import ReactMarkdown from 'react-markdown';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { atomOneLight } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import { useStyles } from './MarkdownFile.style';

export const MarkdownFile = ({ source }) => {
  const classes = useStyles();
  return (
    <ReactMarkdown
      source={source}
      renderers={{
        heading: ({ children, level }) => {
          const heading = `h${level}`;
          const HeadingComponent = heading;
          return (
            <HeadingComponent
              id={children[0].props.value}
              className={classes[heading]}
            >
              {children}
            </HeadingComponent>
          );
        },
        paragraph: ({ children }) => <p className={classes.p}>{children}</p>,
        code: ({ value, language }) => (
          <div className={classes.code}>
            {language === 'tree' ? (
              <pre>{value}</pre>
            ) : (
              <SyntaxHighlighter language={language} style={atomOneLight}>
                {value}
              </SyntaxHighlighter>
            )}
          </div>
        ),
        list: ({ children, ordered }) => {
          const ListComponent = ordered ? 'ol' : 'ul';
          return (
            <ListComponent className={classes.list}>{children}</ListComponent>
          );
        },
        listItem: ({ children }) => (
          <li className={classes.listItem}>{children}</li>
        ),
        blockquote: ({ children }) => (
          <div className={classes.quoteWrapper}>
            <div className={classes.quote}>
              <span className={classes.quoteSign}>"</span>
              {children}
              <span className={classes.quoteSign}>"</span>
            </div>
          </div>
        ),
      }}
    />
  );
};
