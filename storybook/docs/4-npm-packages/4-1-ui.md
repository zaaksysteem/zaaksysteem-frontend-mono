# `@mintlab/ui` developer documentation

> `@mintlab/ui` is an `npm` package with the
> [presentational](https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0)
> React UI components used in Zaaksysteem.

The [API Reference](./identifiers.html) covers component library functions and Storybook utilities.

## `Material-UI` theming

> _Material-UI_ themes are fairly complex; since the
> style guide defines variations on standard concepts
> that are not supported by basic _Material-UI_
> theming, we need a couple of creative workarounds.
> The most important thing to understand is exactly
> _what_ to change _where_, _how_ and _why_.

## Colors

The _Material Design_
[color system](https://material.io/design/color/the-color-system.html#color-usage-palettes)
features has a strong concept of `primary` and `secondary` colors,
each placed in a palette of lighter an darker variants.

Beyond that, generalized color classifications include
_surface_, _background_, _error_, _typography_ and _iconography_.

The _Material-UI_ `theme` object reflects this in a `palette`
property with the three main uniform `PaletteIntention` object properties
`primary`, `secondary` and `error`, each of which with the properties
`light`, `main`, `dark` and `contrastText`.

An important aspect of theming is that you can pass these
`PaletteIntention` property names as a `prop` to components,
like `Button`, `Checkbox` and `Radio`. This automagically takes
care of secondary styling, like `hover` colors.

This imposes the following problems for our style guide:

- user defined `PaletteIntention`s like our `review`
  are ignored by the API
- user defined intention properties, like our
  `darker` and `lighter` values for
  `primary` and `secondary`
  are ignored by the API
- the API decides which intentions are useful in a given
  context; for example, the `Button` component does
  nothing with the `error` intention as value for its
  `color` prop

Trying to reverse engineer these `color` prop effects
manually would be tedious and might become brittle in
the future. That's why we use a _nested theme_ HoC in
this case, mapping the main theme's palette `review`
and `danger` intentions to `primary` and `secondary`
in the nested theme. It is important to understand that
this nests a **single component** in a separate theme,
so for performance reasons do not use this solution if
there is another option.

## CSS API

Most *Component API*s have a distinct CSS API,
that defines keys that you can override with your own
style sheet. The common denominator is `root` for the
root element.

### Global component style

The `theme` object contains an `override` object where you
can alter the default appearance of all components in
the theme. The `override` _key_ is the component name
prefixed with `Mui`. This is the preferred method of global
styling.

Example:

```javascript
const theme = {
  overrides: {
    MuiButton: {
      root: {
        textTransform: 'none',
      },
    },
  },
};
```

## _Material-UI_ documentation

- Component API
  - [MuiThemeProvider](https://material-ui.com/api/mui-theme-provider/)
- Customization
  - [Themes](https://material-ui.com/customization/themes/)
  - [Default Theme](https://material-ui.com/customization/default-theme/)
- Style
  - [Color](https://material-ui.com/style/color/)
